/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    create:(req,res)=>{
        var fname = req.body.fname;
        var lname = req.body.lname;
        console.log(fname);
        console.log(lname);

        Test.create({fname:fname,lname:lname}).exec((err)=>{
            if(err){
                res.send(500,{error:'database error'});
            }
            // res.send({msg:'Success'});
            res.redirect('test');
        });
    },
    test:(req,res)=>{
        res.view('test');
    }

};

